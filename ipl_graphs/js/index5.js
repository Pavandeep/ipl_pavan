var fs = require('fs');
var express = require('express');
var app = express();
var dat = fs.readFileSync('../../matches.json');

var datajson = JSON.parse(dat);

let umpires = {};
for (const obj of datajson) {
    if (obj.umpire1) {
        if (umpires[obj.umpire1]) {
            umpires[obj.umpire1]++;
        }
        else {
            umpires[obj.umpire1] = 1;
        }
    }
}
for (const obj of datajson) {
    if (obj.umpire2) {
        if (umpires[obj.umpire2]) {
            umpires[obj.umpire2]++;
        }
        else {
            umpires[obj.umpire2] = 1;
        }
    }
}


console.log(umpires);


app.get('/', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(umpires);

});
var ser = app.listen(8000);





