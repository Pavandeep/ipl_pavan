var fs = require('fs');
var express = require('express');
var app = express();
var dat = fs.readFileSync('../../matches.json');
var dat1 = fs.readFileSync('../../deliveries.json');

var datajson = JSON.parse(dat);
var datajson1 = JSON.parse(dat1);

let yearids = [];
for (const obj of datajson) {
    if (obj.season == 2016)
        yearids.push(obj.id);
}
//console.log(yearids);

let extraruns = {};
for (const i of yearids) {
    // console.log(i);
    for (const obj1 of datajson1) {
        if (i == obj1.match_id) {
            if (extraruns[obj1.bowling_team]) {
                extraruns[obj1.bowling_team] = extraruns[obj1.bowling_team] + parseInt(obj1.extra_runs);
                //console.log(extraruns[obj1.bowling_team]);
            }
            else {
                extraruns[obj1.bowling_team] = parseInt(obj1.extra_runs);
                //console.log(obj1.extra_runs);
            }

        }

    }
}
console.log(extraruns);

app.get('/', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(extraruns);

});
var ser = app.listen(8000);