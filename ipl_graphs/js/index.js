var fs = require('fs');
var express = require('express');
var app = express();


app.get('/chart1', function (req, res) {
    var dat = fs.readFileSync('../../matches.json');

    var datajson = JSON.parse(dat);

    let yrNumOfMatches = {};
    for (const obj of datajson) {
        if (yrNumOfMatches[obj.season]) {
            yrNumOfMatches[obj.season]++;
        }
        else {
            yrNumOfMatches[obj.season] = 1;
        }
    }
    console.log(yrNumOfMatches);

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(yrNumOfMatches);

});



app.get('/chart2', function (req, res) {
    var fs = require('fs');
var express = require('express');
var app = express();
var dat = fs.readFileSync('../../matches.json');

var datajson = JSON.parse(dat);
let matchesWon = {};

for (const obj of datajson) {
    //     if (matchesWon[obj.season]) {

    // }
    //     else {

    //      matchesWon[obj.season]=myFunc(obj.season);

    //     }

    if (matchesWon[obj.season]) {
        if (matchesWon[obj.season][obj.winner])
            matchesWon[obj.season][obj.winner]++;
        else
            matchesWon[obj.season][obj.winner] = 1;
    }
    else {
        matchesWon[obj.season] = {};
        matchesWon[obj.season][obj.winner] = 1;
    }

}

// function myFunc(year){
//     let teamWin = {};
//     for(const obj of datajson)
//     {
//         if(year==obj.season){
//     if(teamWin[obj.winner])
//      {
//          teamWin[obj.winner]++;
//      }
//      else{
//          teamWin[obj.winner] = 1;
//      }
//     }
//     }


//     return(teamWin);
// }





console.log(matchesWon);

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(matchesWon);

});




app.get('/chart3', function (req, res) {
    var fs = require('fs');
var express = require('express');
var app = express();
var dat = fs.readFileSync('../../matches.json');
var dat1 = fs.readFileSync('../../deliveries.json');

var datajson = JSON.parse(dat);
var datajson1 = JSON.parse(dat1);

let yearids = [];
for (const obj of datajson) {
    if (obj.season == 2016)
        yearids.push(obj.id);
}
//console.log(yearids);

let extraruns = {};
for (const i of yearids) {
    // console.log(i);
    for (const obj1 of datajson1) {
        if (i == obj1.match_id) {
            if (extraruns[obj1.bowling_team]) {
                extraruns[obj1.bowling_team] = extraruns[obj1.bowling_team] + parseInt(obj1.extra_runs);
                //console.log(extraruns[obj1.bowling_team]);
            }
            else {
                extraruns[obj1.bowling_team] = parseInt(obj1.extra_runs);
                //console.log(obj1.extra_runs);
            }

        }

    }
}
console.log(extraruns);

    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(extraruns);

});



app.get('/chart4', function (req, res) {
    var fs = require('fs');
var express = require('express');
var app = express();
var dat = fs.readFileSync('../../matches.json');
var dat1 = fs.readFileSync('../../deliveries.json');

var datajson1 = JSON.parse(dat1);
var datajson = JSON.parse(dat);

let yearids = [];
for (const obj of datajson) {
    if (obj.season == 2015)
        yearids.push(obj.id);
}
//console.log(yearids);

let runsgivenbybowlers = {};
let noofballthrown = {};
for (const i of yearids) {
    // console.log(i);
    for (const obj1 of datajson1) {
        if (i == obj1.match_id) {
            if (runsgivenbybowlers[obj1.bowler]) {
                runsgivenbybowlers[obj1.bowler] = runsgivenbybowlers[obj1.bowler] + parseInt(obj1.total_runs);
                //console.log(runsgivenbybowlers[obj1.bowler]);
                noofballthrown[obj1.bowler]++;
            }
            else {
                runsgivenbybowlers[obj1.bowler] = parseInt(obj1.total_runs);
                noofballthrown[obj1.bowler] = 1;
                //console.log(obj1.extra_runs);
            }

        }

    }
}
//console.log(runsgivenbybowlers);
//console.log(noofballthrown);
let economyofbowler = {};
for (const j in runsgivenbybowlers) {
    //console.log(j);
    if (economyofbowler[j]) {

    } else {
        economyofbowler[j] = (runsgivenbybowlers[j] * 6) / noofballthrown[j];
    }


}
//console.log(economyofbowler);
// let keysSorted;
// keysSorted = Object.keys(economyofbowler).sort(function(a,b){return economyofbowler[a]-economyofbowler[b]})
// console.log(keysSorted); 

function sortObject(obj) {
    var arr = [];
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop]
            });
        }
    }
    arr.sort(function (a, b) { return a.value - b.value; });
    return arr;
}

var arr = sortObject(economyofbowler);
//console.log(arr);


var rv = {};
for (var i = 0; i < arr.length; ++i) {
    rv[arr[i].key] = arr[i].value;

}
console.log(rv);



    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(rv);

});




app.get('/chart5', function (req, res) {
    var fs = require('fs');
var express = require('express');
var app = express();
var dat = fs.readFileSync('../../matches.json');

var datajson = JSON.parse(dat);

let umpires = {};
for (const obj of datajson) {
    if (obj.umpire1) {
        if (umpires[obj.umpire1]) {
            umpires[obj.umpire1]++;
        }
        else {
            umpires[obj.umpire1] = 1;
        }
    }
}
for (const obj of datajson) {
    if (obj.umpire2) {
        if (umpires[obj.umpire2]) {
            umpires[obj.umpire2]++;
        }
        else {
            umpires[obj.umpire2] = 1;
        }
    }
}


console.log(umpires);
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(umpires);

});






var ser = app.listen(8000);








