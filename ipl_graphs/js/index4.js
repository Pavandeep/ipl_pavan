var fs = require('fs');
var express = require('express');
var app = express();
var dat = fs.readFileSync('../../matches.json');
var dat1 = fs.readFileSync('../../deliveries.json');

var datajson1 = JSON.parse(dat1);
var datajson = JSON.parse(dat);

let yearids = [];
for (const obj of datajson) {
    if (obj.season == 2015)
        yearids.push(obj.id);
}
//console.log(yearids);

let runsgivenbybowlers = {};
let noofballthrown = {};
for (const i of yearids) {
    // console.log(i);
    for (const obj1 of datajson1) {
        if (i == obj1.match_id) {
            if (runsgivenbybowlers[obj1.bowler]) {
                runsgivenbybowlers[obj1.bowler] = runsgivenbybowlers[obj1.bowler] + parseInt(obj1.total_runs);
                //console.log(runsgivenbybowlers[obj1.bowler]);
                noofballthrown[obj1.bowler]++;
            }
            else {
                runsgivenbybowlers[obj1.bowler] = parseInt(obj1.total_runs);
                noofballthrown[obj1.bowler] = 1;
                //console.log(obj1.extra_runs);
            }

        }

    }
}
//console.log(runsgivenbybowlers);
//console.log(noofballthrown);
let economyofbowler = {};
for (const j in runsgivenbybowlers) {
    //console.log(j);
    if (economyofbowler[j]) {

    } else {
        economyofbowler[j] = (runsgivenbybowlers[j] * 6) / noofballthrown[j];
    }


}
//console.log(economyofbowler);
// let keysSorted;
// keysSorted = Object.keys(economyofbowler).sort(function(a,b){return economyofbowler[a]-economyofbowler[b]})
// console.log(keysSorted); 

function sortObject(obj) {
    var arr = [];
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            arr.push({
                'key': prop,
                'value': obj[prop]
            });
        }
    }
    arr.sort(function (a, b) { return a.value - b.value; });
    return arr;
}

var arr = sortObject(economyofbowler);
//console.log(arr);


var rv = {};
for (var i = 0; i < arr.length; ++i) {
    rv[arr[i].key] = arr[i].value;

}
console.log(rv);




app.get('/', function (req, res) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE'); // If needed
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type'); // If needed
    res.setHeader('Access-Control-Allow-Credentials', true); // If needed


    res.send(rv);

});
var ser = app.listen(8000);





